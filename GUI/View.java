import javax.swing.*;
import java.beans.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class View implements ActionListener, PropertyChangeListener {

	private JFrame frame = new JFrame("Thread Sorts Demo");
	private JPanel pane = new JPanel(new GridLayout(6,1));

	private JLabel inputLabel = new JLabel("Enter number between 1000 and 1000000");
	private JButton go = new JButton("Go");
	private JLabel finished = new JLabel("Process Status");

	private JTextField input = new JTextField();

	private JPanel inputPanel = new JPanel(new FlowLayout());

	private JPanel insertionPanel = new JPanel(new FlowLayout());

	private JProgressBar insertionBar = new JProgressBar();

	private JPanel quickPanel = new JPanel(new FlowLayout());

	private JProgressBar quickBar = new JProgressBar();

	private int userInput;

	private InsertionSortDemo insertionSort;
	private QuickSortDemo quickSort;

	private Integer[] data;
	private Integer[] insertionData;
	private Integer[] quickData;

	private JLabel insertionLabel = new JLabel("Insertion Sort:");
	private JLabel quickLabel = new JLabel("Quick Sort:");

	public View() {
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);

		input.setPreferredSize(new Dimension(150, 25));
		inputPanel.add(inputLabel);
		inputPanel.add(input);
		inputPanel.add(go);
		insertionPanel.add(insertionLabel);
		insertionPanel.add(insertionBar);
		quickPanel.add(quickLabel);
		quickPanel.add(quickBar);

		pane.add(inputPanel);
		pane.add(insertionPanel);
		pane.add(quickPanel);
		pane.add(finished);

		go.addActionListener(this);

		frame.add(pane);
		frame.pack();
		frame.setVisible(true);
		frame.setResizable(false);
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		if(event.getSource() == go) {
			try {
				if(Integer.parseInt(input.getText()) > 1000000 || Integer.parseInt(input.getText()) < 1000)
					JOptionPane.showMessageDialog(null, "Enter number between 1000 and 1000000");
				else {
					userInput = Integer.parseInt(input.getText());
					createArray();
					runThreads();
				}
			}
			catch(java.lang.NumberFormatException e) {
				JOptionPane.showMessageDialog(null, "Invalid range");
			}
		}
	}

	private void createArray() {
		data = new Integer[userInput];

		Random generator = new Random();
		for(int x = 0; x < data.length; x++) {
			data[x] = generator.nextInt(1000000);
		}
		insertionData = data.clone();
		quickData = data.clone();
	}

	private void runThreads() {
		insertionSort = new InsertionSortDemo(insertionData);
		quickSort = new QuickSortDemo(quickData);

		insertionSort.addPropertyChangeListener(this);
		quickSort.addPropertyChangeListener(this);

		insertionSort.execute();
		quickSort.execute();
	}

	public void propertyChange(PropertyChangeEvent evt) {
		if(evt.getSource().equals(insertionSort)) {
			if("progress" == evt.getPropertyName()) {
				int progress = (Integer) evt.getNewValue();
				if(progress == 100) {
					insertionBar.setValue(progress);
					finished.setText("Process Status: complete");
				}
				else {
					finished.setText("Process Status: running");
					insertionBar.setValue(progress);
				}
			}
		}
		else if(evt.getSource().equals(quickSort)) {
			if("progress" == evt.getPropertyName()){
				int progress = (Integer) evt.getNewValue();
				if(progress == 100) {
					quickBar.setValue(progress);
					finished.setText("Process Status: complete");
				}
				else {
					finished.setText("Process Status: running");
					quickBar.setValue(progress);
				}
			}
		}
	}
}