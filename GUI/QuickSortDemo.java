import javax.swing.*;

public class QuickSortDemo extends SwingWorker {
	private Integer[] data;
	private int numberOfPasses;

	public QuickSortDemo(Integer[] data) {
		super();
		this.data = data;
	}

	@Override
	public Void doInBackground() {
		quickSort(data, 0, data.length - 1);
		return null;
	}

	@Override
	public void done() {
		try {
			setProgress(100);
			get();
		}
		catch(Exception e)
		{
			System.out.println("Error while running in background");
			e.printStackTrace();
		}
	}

	private void quickSort(Integer[] data, int min, int max) {
		if(min < max) {
			int indexOfPartition = partition(data, min, max);
			quickSort(data, min, indexOfPartition - 1);
			quickSort(data, indexOfPartition + 1, max);
		}
		numberOfPasses++;
		updateProgress();
	}

	private int partition(Integer[] data, int min, int max) {
		Integer partitionElement;
		int left, right;
		int middle = (min + max) / 2;
		partitionElement = data[middle];

		swap(data, middle, min);

		left = min;
		right = max;

		try {
			while(left < right) {
				while(left < right && data[left].compareTo(partitionElement) <= 0)
					left++;
				while(data[right].compareTo(partitionElement) > 0)
					right--;
				if(left < right)
					swap(data, left, right);
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		swap(data, min, right);
		return right;
	}

	private void swap(Integer[] data, int index1, int index2) {
		Integer temp = data[index1];
		data[index1] = data[index2];
		data[index2] = temp;
	}

	private void updateProgress() {
		int result;
		double progressCount = 1 - (((double)data.length - (double)numberOfPasses)/(double)data.length);
		result = (int) (progressCount * 100);
		if(result > 100)
			result = 100;
		setProgress(result);
	}
}