import javax.swing.*;

public class InsertionSortDemo extends SwingWorker {

	private Integer[] data;

	public InsertionSortDemo(Integer[] data) {
		super();
		this.data = data;
	}

	@Override
	public Void doInBackground() {
		insertionSort();
		return null;
	}

	@Override
	public void done() {
		try {
			setProgress(100);
			get();
		}
		catch(Exception e) {
			System.out.println("Error while running in the background");
			e.printStackTrace();
		}
	}

	private void insertionSort() {
		for(int index = 1; index < data.length; index++) {
			int key = data[index];
			int position = index;
			updateProgress(index);

			while(position > 0 && data[position-1].compareTo(key) > 0) {
				data[position] = data[position-1];
				position--;
			}
			data[position] = key;
		}
	}

	private void updateProgress(int numberOfPasses) {
		int result;
		double progressCount = 1 - ( ((double)data.length - (double)numberOfPasses)/(double)data.length );
		result = (int)(progressCount * 100);
		setProgress(result);
	}
}