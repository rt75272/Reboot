#include <stdio.h>
/**
 *Using getchar() and putchar(), convert input to output
 *Count number of characters
 */
int main(void) {
	int c;
	int charNum = 0;

	while( (c = getchar()) != EOF) {
		putchar(c);
		++charNum;
		// printf("%d\n", c);
		// printf("%d\n", EOF);
	}
	printf("%d\n", EOF);
	printf("Number of characters:\t%1d\n", charNum);
}