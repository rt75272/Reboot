#include <stdio.h>

//Define symbolic constants
#define LOWER 0 
#define UPPER 300
#define STEP 20

/**
 *Prints a chart of Fahrenheit to Celsius convertions
 */
int main(void) {
	float celsius = 0.0;

	for(float fahrenheit = LOWER; fahrenheit <= UPPER; fahrenheit+=STEP) {
		celsius = (5.0/9.0) * (fahrenheit-32.0); //dividing by must happen after to avoid integer division making 5/9 == 0. Unless its a float.
		printf("%8.2f %13.2f\n", fahrenheit, celsius); //print formatted to handle both variables.  
	}	
	return 0;
}